<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class team extends Model
{
    protected $table = 'teams';
    protected $primaryKey = 'id_team';


    public function users()
    {
        return $this->hasOne('App\User', 'id', 'id_team');
    
    }
}

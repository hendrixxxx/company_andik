<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portofolio extends Model
{
    protected $table = 'portofolios';
    protected $primaryKey = 'id_porto';


    public function gambar()
    {
        return $this->hasMany('App\Gambar', 'parent_id_porto', 'id_porto');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'perent_id_user');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gambar extends Model
{
    protected $table = 'gambars';
    protected $primaryKey = 'id_gambar';


    public function kategori()
    {
        return $this->hasOne('App\Kategori', 'id_kategori', 'parent_id_kategori');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'parent_id_user');
    
    }
    public function porto()
    {
        return $this->hasOne('App\Portofolio', 'id_porto', 'parent_id_porto');
    }


}

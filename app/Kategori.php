<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    protected $table = 'kategoris';
    protected $primaryKey = 'id_kategori';


    public function gambar()
    {
        return $this->hasMany('App\Gambar', 'parent_id_kategori', 'id_kategori');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'id_user');
    
    }
}

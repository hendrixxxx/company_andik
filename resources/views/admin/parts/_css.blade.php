<!-- Bootstrap core CSS -->
<link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">

<!-- Add custom CSS here -->
<link href="{{asset('assets/css/sb-admin.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}">
<!-- Page Specific CSS -->
<!-- <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css"> -->

@stack('css')
@stack('style')

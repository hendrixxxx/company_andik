  <!-- Sidebar -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html">Halaman Admin</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse navbar-ex1-collapse">
          <ul class="nav navbar-nav side-nav">
              <li class="active"><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>

              <li class="active"><a href="portofolio.html"><i class="fa fa-edit"></i> Portofolio</a></li>
              <li class="active"><a href="foto.html"><i class="fa fa-edit"></i> Gallery</a></li>

          </ul>
          </li>
          </ul>

          <ul class="nav navbar-nav navbar-right navbar-user">
              <li class="dropdown messages-dropdown">

              <li class="dropdown user-dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                  <ul class="dropdown-menu">

                      <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                      <li class="divider"></li>
                      <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                  </ul>
              </li>
          </ul>
      </div><!-- /.navbar-collapse -->
  </nav>
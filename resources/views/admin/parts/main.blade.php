<!DOCTYPE html>
<html lang="en">

<head>

    @include('admin.parts.header')

    @include('admin.parts._css')

</head>

<body>

    <div id="wrapper">

        @include('admin.parts.side')

        <div id="page-wrapper">

            @yield('content')

        </div>
    </div>
    </div>
    </div><!-- /.row -->

    </div><!-- /#page-wrapper -->

    </div><!-- /#wrapper -->

    @include('admin.parts._js')


</body>

</html>
@extends('admin.parts.main')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1>Portofoliso <small>Enter Your Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-edit"></i> Portofolio</li>
            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
                Launch Default Modal
            </button>
        </ol>

    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered" id="table_kategori">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                </tr>
            </thead>
        </table>
    </div>
</div><!-- /.row -->

<div class="modal fade" id="modal-default" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Default Modal</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>One fine body…</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal-content -->
@endsection

@push('css')
{{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"> --}}
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@endpush

@push('js')
<!-- DataTables -->
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
@endpush

@push('script')
<script>
    const $table = $("#table_kategori").DataTable({
        responsive: true,
        autoWidth: false,
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: '{!! route("source.kategori") !!}',
            dataSrc: 'data'
        },
        language: {
            paginate: {
                previous: "<i class='fa fa-chevron-left'></i>",
                next: "<i class='fa fa-chevron-right'></i>",
            }
        },
        columns: [
            // {data: 'DT_RowIndex', name: 'DT_RowIndex',width:"2%", orderable : false, searchable : false, className: 'text-center'},
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false
            },
            {
                data: 'nama_kategori',
                name: 'nama_kategori'
            },
            // {data: 'action', name: 'action', searchable : false, orderable : false, className: 'text-center'}
        ],
        columnDefs: [{
            width: "10%",
            targets: 0
        }],
        lengthMenu: [
            [5, 10, 25, 50],
            [5, 10, 25, 50]
        ],
        search: {
            "regex": true
        }
    });

    function store(val) {
        var data = $('#form_data').serialize();

        $.ajax({
            type: 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '{!!route("kategori.store")!!}',
            dataType: 'json',
            data: data,
            async: true,
            beforeSend: function() {
                $('#submit').prop('disabled', true);
            },
            success: function(data) {
                $('#modal-w').modal('toggle');
                $table.ajax.reload()
            },
            error: function(data) {
                $('#submit').prop('disabled', false);
                if (data.status === 422) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors['errors'], function(index, value) {
                        $('#form_create').Toasts('create', {
                            title: 'Error Input' + index,
                            autohide: true,
                            class: 'bg-danger',
                            delay: 2500,
                            body: value

                        })
                    });

                }
            },
        });
    }
</script>
@endpush
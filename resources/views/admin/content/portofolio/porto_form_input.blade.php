@extends('admin.parts.main')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1>Portofoliso <small>Enter Your Data</small></h1>
        <ol class="breadcrumb">
            <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active"><i class="fa fa-edit"></i> Portofolio</li>
        </ol>

    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-lg-6">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default">
            add Image
        </button>
    </div>
</div><!-- /.row -->
<form role="form" action="">
    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>Kategori</label>
                        <select class="form-control">
                            <option>option 1</option>
                            <option>option 2</option>
                            <option>option 3</option>
                            <option>option 4</option>
                            <option>option 5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>File input</label>
                        <input type="file">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit Button</button>
                    <button type="reset" class="btn btn-default">Reset Button</button>
                </div>
            </div>
</form>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
@endsection


@push('style')
<link rel="stylesheet" href="{{asset('assets/css/dropzone.css')}}">
@endpush
@push('js')
<script src="{{asset('assets/js/dropzone.js')}}"></script>
@endpush
@push('script')
<script>
    Dropzone.options.dropzone =
    {
        maxFilesize: 2,
        maxFiles: 1,
        init: function ()  {
            this.on("error", function (file, message) {
                $(document).Toasts('create', {                 
                    title: 'Error',
                    autohide: true,
                    class: 'bg-danger',
                    delay: 2500,
                    body: message
            
                });
                this.removeFile(file);
            }); 
        },
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        addRemoveLinks: true,
        timeout: 5000,
        success: function(file, response) 
        {
            console.log(response);
        },
        error: function(file, response)
        {
            return false;
        }
    };
</script>
@endpush
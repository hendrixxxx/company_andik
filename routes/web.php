<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.content.kategori.kategori');
});

Route::resource('/kategori', 'Kategori_controller');
Route::get('/source/kategori', 'Kategori_controller@datatables')->name('source.kategori');

